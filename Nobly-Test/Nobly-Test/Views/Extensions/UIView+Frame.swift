//
//  UIView+Frame.swift
//  Nobly-Test
//
//  Created by Gastón Montes on 23/12/2021.
//

import Foundation
import UIKit

extension UIView {
    var matrixFrameHeight: CGFloat {
        return self.frame.size.height
    }
}
