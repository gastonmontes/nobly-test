//
//  AddMatrixViewController.swift
//  Nobly-Test
//
//  Created by Gastón Montes on 22/12/2021.
//

import UIKit

private let kAddMatrixTitleKey = "AddMatrixViewController.title"
private let kAddMatrixCancelButtonTitleKey = "AddMatrixViewController.cancel.title"

class AddMatrixViewController: BaseViewController {
    // MARK: - IBOutlets vars.
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var navigationBarTitleLabel: UILabel!
    @IBOutlet weak var navigationBarCancelButton: UIButton!
    
    // MARK: - View life cycle.
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupViews()
    }
    
    // MARK: - Views setup.
    private func setupViews() {
        self.setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        self.navigationBarView.backgroundColor = .matrixNavigationBarBackgroundColor
        
        self.navigationBarTitleLabel.text = kAddMatrixTitleKey.stringLocalized()
        self.navigationBarTitleLabel.textColor = .matrixNavigationBarTintColor
        self.navigationBarTitleLabel.font = .matrixNavigationBarBarTitleFont
        
        self.navigationBarCancelButton.setTitle(kAddMatrixCancelButtonTitleKey.stringLocalized(), for: .normal)
        self.navigationBarCancelButton.setTitleColor(.matrixNavigationBarTintColor, for: .normal)
        self.navigationBarCancelButton.titleLabel?.font = .matrixNavigationBarBarTitleFont
    }
    
    // MARK: - Actions functions.
    @IBAction func onCancelAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
