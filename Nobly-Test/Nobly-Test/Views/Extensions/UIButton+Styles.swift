//
//  UIButton+Styles.swift
//  Nobly-Test
//
//  Created by Gastón Montes on 23/12/2021.
//

import Foundation
import UIKit

extension UIButton {
    func matrixSetButtonEnabledStyle(title: String) {
        self.setTitleColor(.matrixCalculateViewTintColor, for: .normal)
        self.setTitle(title, for: .normal)
        self.matrixBorder(withRadius: self.matrixFrameHeight / 2,
                          borderWidth: 2,
                          borderColor: .matrixCalculateViewTintColor)
        self.titleLabel?.font = .matrixCalculateButtonTitleFont
        self.isEnabled = true
    }
    
    func matrixSetButtonDisabledStyle(title: String) {
        self.setTitleColor(.matrixCalculateViewDisabledColor, for: .disabled)
        self.setTitle(title, for: .normal)
        self.matrixBorder(withRadius: self.matrixFrameHeight / 2,
                          borderWidth: 2,
                          borderColor: .matrixCalculateViewDisabledColor)
        self.titleLabel?.font = .matrixCalculateButtonTitleFont
        self.isEnabled = false
    }
}
