//
//  BaseViewController.swift
//  Nobly-Test
//
//  Created by Gastón Montes on 22/12/2021.
//

import UIKit

class BaseViewController: UIViewController {
    // MARK: - View life cycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupNavBar()
    }
    
    // MARK: - Views setup.
    private func setupNavBar() {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = .matrixNavigationBarBackgroundColor
        self.navigationController?.navigationBar.tintColor = .matrixNavigationBarTintColor
        self.navigationController?.navigationBar.standardAppearance = appearance;
        self.navigationController?.navigationBar.scrollEdgeAppearance = appearance
    }
}
