//
//  UIView+Borders.swift
//  Nobly-Test
//
//  Created by Gastón Montes on 23/12/2021.
//

import Foundation
import UIKit

extension UIView {
    func matrixBorder(withRadius cornerRadius: CGFloat, borderWidth: CGFloat, borderColor: UIColor) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
}
