//
//  MatrixViewController.swift
//  Nobly-Test
//
//  Created by Gastón Montes on 21/12/2021.
//

import UIKit

private let kMatrixTitleKey = "MatrixViewController.title"
private let kMatrixCalculateButtonTitleKey = "MatrixViewController.calculate.title"

class MatrixViewController: BaseViewController {
    // MARK: - IBOutlets vars.
    
    @IBOutlet weak var calculateContainerView: UIView!
    @IBOutlet weak var calculateButton: UIButton!
    
    // MARK: - View life cycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    // MARK: - Views setup.
    private func setupView() {
        self.setupnavigationBar()
        self.setupCalculateView()
    }
    
    private func setupnavigationBar() {
        self.title = kMatrixTitleKey.stringLocalized()
    }
    
    private func setupCalculateView() {
        self.calculateContainerView.backgroundColor = .matrixCalculateViewBackgroundColor
        
        self.calculateButton.matrixSetButtonDisabledStyle(title: kMatrixCalculateButtonTitleKey.stringLocalized())
    }
    
    // MARK: - Actions functions.
    @IBAction private func onAddMatrixAction(_ sender: UIBarButtonItem) {}
}
