//
//  String+Localized.swift
//  Nobly-Test
//
//  Created by Gastón Montes on 22/12/2021.
//

import Foundation

extension String {
    func stringLocalized() -> String {
        return NSLocalizedString(self, comment: "")
    }
}
