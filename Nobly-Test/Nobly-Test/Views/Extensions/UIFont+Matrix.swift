//
//  UIFont+Matrix.swift
//  Nobly-Test
//
//  Created by Gastón Montes on 22/12/2021.
//

import Foundation
import UIKit

extension UIFont {
    class var matrixNavigationBarBarTitleFont : UIFont {
        return UIFont.systemFont(ofSize: 17, weight: .semibold)
    }
    
    class var matrixCalculateButtonTitleFont : UIFont {
        return UIFont.systemFont(ofSize: 17, weight: .bold)
    }
}
