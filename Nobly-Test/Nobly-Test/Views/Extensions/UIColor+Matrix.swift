//
//  UIColor+Matrix.swift
//  Nobly-Test
//
//  Created by Gastón Montes on 21/12/2021.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int, alpha: Float) {
        let redFloat = CGFloat(red)/255
        let greenFloat = CGFloat(green)/255
        let blueFloat = CGFloat(blue)/255
        
        self.init(red: redFloat, green: greenFloat, blue: blueFloat, alpha: 1.0)
    }
    
    class var matrixYellowColor: UIColor {
        return UIColor(red: 251, green: 233, blue: 44, alpha: 1.0)
    }
    
    class var matrixBlackColor: UIColor {
        return UIColor.black
    }
    
    // MARK: - Navigation bar colors.
    class var matrixNavigationBarBackgroundColor : UIColor {
        return .matrixYellowColor
    }
    
    class var matrixNavigationBarTintColor : UIColor {
        return .matrixBlackColor
    }
    
    // MARK: - Matrix view controller colors.
    class var matrixCalculateViewBackgroundColor : UIColor {
        return .matrixYellowColor
    }
    
    class var matrixCalculateViewTintColor : UIColor {
        return .matrixBlackColor
    }
    
    class var matrixCalculateViewDisabledColor : UIColor {
        return .gray
    }
}

